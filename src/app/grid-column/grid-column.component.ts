import { Component, OnInit, Input, ViewChild, ElementRef, Renderer2, Output, EventEmitter } from '@angular/core';
import {Day, Interval, PatientRecord, User} from '../model'
import { DatePipe } from '@angular/common';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../data.service';

@Component({
  selector: 'app-grid-column',
  templateUrl: './grid-column.component.html',
  styleUrls: ['./grid-column.component.scss'],
  providers: [DatePipe]
})
export class GridColumnComponent implements OnInit {

  @Input() columns: Day[];
  @Input() patient: User;
  // @ViewChild('contextmenumodal') contextmenumodal: NgbModalRef;
  
  @Output() changeGrid = new EventEmitter<true>();
  updateGrid() { 
    this.changeGrid.emit();
  }

  contextText:string;
  currentInterval:Interval;
  contextMenuModal: any;
  cancelModal: any;
  currentRecord: PatientRecord;
  room: string;

  constructor(private renderer: Renderer2, 
              private datepipe: DatePipe, 
              private modalService: NgbModal,
              private dataService: DataService) { }

  ngOnInit() {
  }

  openContextMenu(e, int:Interval, user:PatientRecord, modal, room) {
    e.preventDefault();
    e.stopPropagation();
    if (int.isAppointment || int.isBusy) {
      this.contextMenuModal = this.modalService.open(modal, { size : "sm", windowClass: 'modal-menu' })
      this.currentInterval = int;
      this.currentRecord = user;
      this.room = room;
      if (int.isBusy) {
        this.contextText = user.patient.surname + " " + user.patient.name + " " + user.patient.middlename;
      } else {
        this.contextText = this.datepipe.transform(int.interval,"HH:mm") + " - " + 
        this.datepipe.transform(int.intervalEnd,"HH:mm");
      }      
    }
    return false;
  }

  showInfo(modal) {
    this.contextMenuModal.dismiss();
    this.modalService.open(modal, { size: 'lg', windowClass: 'modal-info'});
  }

  createRec(modal) {
    let obj = {
      patient: this.patient,
      interval: {start: this.currentInterval.date, end: null, days: []},
      spec: this.currentInterval.spec
    };
    this.dataService.saveRecord(obj).then(response => {
      if (response.status === 201) {
        this.modalService.open(modal, { size : "sm", windowClass: 'modal-success'});
        this.contextMenuModal.dismiss();
        this.updateGrid();        
      }
    })
  }

  deleteRec(modal) {
    this.contextMenuModal.dismiss();
    this.cancelModal = this.modalService.open(modal, { size : "sm", windowClass: 'modal-cancel'});    
  }

  cancel() {
    this.dataService.deleteRecord(this.currentRecord.id).then(response => {
      console.log(response)
      if (response.status === 204) {
        this.cancelModal.dismiss();
        this.updateGrid();        
      }
    })
  }
}
