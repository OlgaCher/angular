import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import {NgbCalendar, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map} from 'rxjs/operators';
import { DataService} from '../data.service';
import { InternalFormsSharedModule } from '@angular/forms/src/directives';
import {User, Specialist, Position} from '../model'

@Component({
  selector: 'app-side-panel',
  templateUrl: './side-panel.component.html',
  styleUrls: ['./side-panel.component.scss', '../app.component.scss'],
  providers: [DataService]
})
export class SidePanelComponent implements OnInit {

  public patient: User = null;
  patients:User[] = [];
  positions:Position[] = [];

  patientSelected:boolean = false;

  specialists:Specialist[] = [];
  public specialist:Specialist = null;

  DRList = [];
  
  selectedDate: NgbDateStruct;
  today = this.calendar.getToday();

  @Output() onChangeDate = new EventEmitter<NgbDateStruct>();
  changeDate() { 
    this.onChangeDate.emit(this.selectedDate);
  }

  @Output() onChangePatient = new EventEmitter<User>();
  changePatient() { 
    this.onChangePatient.emit(this.patient);
  }

  @Output() onChangeSpecialists = new EventEmitter<Specialist[]>();
  changeSpecialists(i) { 
    if (i !== null && !this.DRList[i].isSpecialist) {
      let j = i+1;
      while (this.DRList[j].isSpecialist === true) {
        this.DRList[j].checked = !this.DRList[i].checked;
        j++;
      }
    }
    this.onChangeSpecialists.emit(this.DRList);
  }

  constructor(private calendar: NgbCalendar, private dataService: DataService) { }

  ngOnInit() {
    let promises = [];
    this.dataService.getPatients().then((data) => {
      this.patients = data.data;
    });

    promises.push(this.dataService.getPositions().then((data) => {
      this.positions = data.data;
    }));

    promises.push(this.dataService.getSpecialists().then((data) => {
      //this.specialists = data.data.map(dt => ({id: dt.id, name: dt.name, position: dt.position, checked: false}));
      this.specialists = data.data;
      this.specialists.sort((a:Specialist, b:Specialist) => a.name.localeCompare(b.name));
    }));

    Promise.all(promises).then(() => {
      this.changeSort(true);
    });
  }

  ngAfterViewChecked() {
    let hWindow = document.documentElement.offsetHeight;  // высота окна
    let hBtn = document.getElementById('btnGroup').offsetHeight + document.getElementById('btnGroup').offsetTop + 
    document.getElementById('rowSpecialist').offsetTop;
    document.getElementById('scrollSpecialist').style.height = (hWindow - hBtn - 70).toString() + "px";
}

  formatter = (user: User) => user.name + ", " + user.policy;

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map((term:string) => term.length < 3 ? []
        : this.patients.filter((v:User) => {
          if (isNaN(Number(term[0]))) {
            return v.name.toLowerCase().indexOf(term.toLowerCase()) == 0;
          } else {
            return v.policy.indexOf(term.toLowerCase()) == 0;
          }
        }).slice(0, 10))
    );

  isString(val) { return typeof val === 'string'; } 
  
  completePatient() {
    this.patientSelected = true;
    this.changePatient();
  }

  formatterSpec = (user: User) => user.name;

  searchSpec = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map((term:string) => term.length < 3 ? []
        : this.specialists.filter((v:Specialist) => {
          return (v.name.toLowerCase().indexOf(term.toLowerCase()) == 0 || 
          v.position.name.toLowerCase().indexOf(term.toLowerCase()) == 0);
        }).slice(0, 10))
    );

  countCheckedSpec():number {
    return this.DRList.reduce((a,b) => {if (b.checked && b.isSpecialist) a = a+1; return a;}, 0);
  }

  allSelected() {
    this.DRList.forEach(function (item){
      item.checked = true;
    });
    this.changeSpecialists(null);
  }

  cancelSelected() {
    this.DRList.forEach(function (item){
      item.checked = false;
    });
    this.changeSpecialists(null);
  }

  changeSort(isSpecSort) {
    this.DRList = [];
    if (isSpecSort) {
     this.positions.forEach(pos => {
      this.DRList.push({
        isFirstLevel: true,
        obj: pos,
        checked: false,
        isSpecialist: false
      });
        this.specialists.forEach((spec:Specialist) => {
          if (spec.position.id === pos.id) {
            this.DRList.push({
              isFirstLevel: false,
              obj: spec,
              checked: false,
              isSpecialist: true
            });         
          }
        });
      });
    } else {
      this.specialists.forEach((spec:Specialist) => {
        this.DRList.push({
          isFirstLevel: true,
          obj: spec,
          children: [],
          isSpecialist: true
        });
      }); 
    }
    this.changeSpecialists(null);
  }
}
