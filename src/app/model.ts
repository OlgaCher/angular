  export interface Day {
    name: string,
    dayOfWeek: number,
    fullDate: Date
  }
  
  export interface DR {
    spec: Specialist, 
    position: Position,
    room: string,
    startWorkTime: Date,
    endWorkTime: Date,
    days: number[],
    gridStep: number,
    appointment: Quota[],
    dontWork: Quota[],
    learn: Quota[],
    docWork: Quota[],
    vacation: Quota[],
    homeAppointment: Quota[],
    sickLeave: Quota[]
  }
  
  export interface User {
    id: number,
    surname:string;
    name:string;
    middlename:string;
    date:string;
    policy:string;
  }

  // export interface Doctor {
  //   id:number,
  //   name:string
  // }
  
  export interface Position {
    id:number,
    name:string
  }
  
  export interface Specialist {
    id: number,
    name:string,
    position: Position,
    checked:boolean
  }
  
  export interface Quota {
    start:Date,
    end:Date,
    days:number[]
  }

  export interface Interval {
    interval:string,
    intervalEnd: string,
    date:Date,
    spec:Specialist,
    name:string,
    patients:PatientRecord[],
    isAppointment:boolean,
    isDontWork:boolean,
    isLearn:boolean,
    isDocWork:boolean,
    isVacation:boolean,
    isHomeAppointment:boolean,
    isSickLeave:boolean,
    isBusy:boolean
  }

  export interface Column {
    today: string,
    spec: Specialist,
    position: Position,
    room: string,
    startWorkTime: Date,
    endWorkTime: Date,
    days: number[],
    intervals: Interval[]
  }

  export interface PatientRecord {
    id: number,
    patient: User,
    spec: Specialist,
    interval: Quota,
    //room: string
  }