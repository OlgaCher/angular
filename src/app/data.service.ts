import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { PatientRecord } from './model';

@Injectable()
export class DataService {

  constructor(private http: Http) { }

  getPatients() : Promise<any> {
    return this.http.get(`/api/patients`).toPromise().then(response => response.json());
  }

  getPositions() : Promise<any> {
    return this.http.get(`/api/positions`).toPromise().then(response => response.json());
  }

  getSpecialists() : Promise<any> {
    return this.http.get(`/api/specialists`).toPromise().then(response => response.json());
  }

  getSchedule() : Promise<any> {
    return this.http.get(`/api/schedule`).toPromise().then(response => response.json());
  }

  getRecords() : Promise<any> {
    return this.http.get(`/api/records`).toPromise().then(response => response.json());
  }

  saveRecord(record) : Promise<any> {
    return this.http.post(`/api/records`, record).toPromise();
  }

  deleteRecord(id) : Promise<any> {
    return this.http.delete(`/api/records/${id}`).toPromise();
  }


}
