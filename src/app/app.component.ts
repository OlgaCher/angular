import { Component, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import {Day, DR, Column, Interval, Quota, PatientRecord, User, Specialist} from './model'
import { DataService } from './data.service';
import { interval } from 'rxjs/observable/interval';
import { GridColumnComponent } from './grid-column/grid-column.component';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [DataService]
})
export class AppComponent {
  
  @ViewChild('gridcolumn') gridcolumn: GridColumnComponent;
  
  days:Day[] = [];
  schedule:DR[] = [];
  columns:Column[] = [];
  records:PatientRecord[] = [];
  numDays:number = 1;
  curPatient:User = null;

  selectedDate: moment.Moment;
  selectedSpecialists:Specialist[] = [];

  constructor(private dataService: DataService) {};


  ngOnInit() {
    this.updateGrid();
  }

  setPatient(user:User) {
    this.curPatient = user;
  }

  setSpecialists(specialists) {
    this.selectedSpecialists = specialists.filter(obj => obj.isSpecialist && obj.checked)
                                          .map(obj => obj.obj);
    this.updateGrid();
  }

  updateGrid() {
    let promises = [];
    promises.push(this.dataService.getSchedule().then((data) => {
      this.schedule = data.data;
    }));
    promises.push(this.dataService.getRecords().then((data) => {
      this.records = data.data;
    }));
    Promise.all(promises).then(() => {
      this.changeDaysRange(this.numDays);
    });    
  }

  changeDateDaysRange(date:NgbDateStruct) {
    this.selectedDate = moment(date).subtract(1,'month');
    this.changeDaysRange(this.numDays);
  }

  changeDaysRange(num_days:number) {
    moment.locale('ru');
    this.numDays = num_days;
    this.days = [];  
    for (let i = 0; i < this.numDays; i++) {
      let curDay = moment(this.selectedDate).add(i, 'days');
      this.days.push({name: curDay.format('dd. D MMM'), 
                      dayOfWeek: (Number)(curDay.format('e'))+1, 
                      fullDate: curDay.toDate()});
    }
    this.getColumns();
  }

  checkFeature(quota:Quota[], dayOfWeek:number, start:object, end:object, intervals:Interval[], qName:string, str:string) {
    let feature = false;
    let intervalNote = "";
    let check = true;

    if (quota) {
      quota.forEach(function(dw) {
        if (dw.days.includes(dayOfWeek) && 
            moment(dw.start).diff(start) <= 0 &&
            moment(dw.end).diff(end) >= 0) {
              feature = true;
              // если это первая строка интревала, выводим надпись
              if (intervals[intervals.length - 1][qName] === false) {
                intervalNote = str;
              } else {
                intervalNote = "";
                // если это третья и последующая строка интервала, не выводим
                // if (intervals[intervals.length - 1].name === "") {
                  check = false;
                // }
              }
        }
      })
    }
    return {feature: feature, intervalNote: intervalNote, check: check};
  }

  getColumns() {
    this.columns = [];
    this.days.forEach(function(day:Day) {
      this.schedule.forEach(function(sch:DR) {
        if (sch.days.includes(day.dayOfWeek)) {
          let checkDR = this.selectedSpecialists.some((spec:Specialist) => spec.id === sch.spec.id);
          if (checkDR) {
            let intervals:Interval[] = [];
            // количество интервалов
            let num = moment(sch.endWorkTime).diff(moment(sch.startWorkTime)) / (sch.gridStep * 60 * 1000);
            
            for (let i = 0; i < num; i++) {
              // начало интервала
              let start = moment(sch.startWorkTime).add(i * sch.gridStep, 'minute');
              let end = start.clone().add(sch.gridStep, 'minute');
              let intervalNote = "";
              let patients:PatientRecord[] = [];
              let check = true;
              let isAppointment = false;              
              if (sch.appointment) {
                sch.appointment.forEach(function(dw) {
                  if (dw.days.includes(day.dayOfWeek) && 
                      moment(dw.start).diff(start) <= 0 &&
                      moment(dw.end).diff(end) >= 0) {
                        isAppointment = true;
                  }
                })
              }
              if (intervals.length > 0 && intervals[intervals.length - 1].isAppointment === false && !isAppointment) {
                check = false;
              }

              let isDontWork = false;
              let info = this.checkFeature(sch.dontWork, day.dayOfWeek, start, end, 
                intervals, 'isDontWork', "Врач не работает");
              isDontWork = info.feature;
              intervalNote = intervalNote === "" ? info.intervalNote : intervalNote;
              check = check && info.check;

              let isLearn = false;
              info = this.checkFeature(sch.learn, day.dayOfWeek, start, end, 
                intervals, 'isLearn', "Обучение");
              isLearn = info.feature;
              intervalNote = intervalNote === "" ? info.intervalNote : intervalNote;
              check = check && info.check;

              let isDocWork = false;
              info = this.checkFeature(sch.docWork, day.dayOfWeek, start, end, 
                intervals, 'isDocWork', "Работа с документами");
              isDocWork = info.feature;
              intervalNote = intervalNote === "" ? info.intervalNote : intervalNote;
              check = check && info.check;

              let isVacation = false;
              info = this.checkFeature(sch.vacation, day.dayOfWeek, start, end, 
                intervals, 'isVacation', "Отпуск");
              isVacation = info.feature;
              intervalNote = intervalNote === "" ? info.intervalNote : intervalNote;
              check = check && info.check;

              let isHomeAppointment = false;
              info = this.checkFeature(sch.homeAppointment, day.dayOfWeek, start, end, 
                intervals, 'isHomeAppointment', "Прием на дому");
              isHomeAppointment = info.feature;
              intervalNote = intervalNote === "" ? info.intervalNote : intervalNote;
              check = check && info.check;

              let isSickLeave = false;
              info = this.checkFeature(sch.sickLeave, day.dayOfWeek, start, end, 
                intervals, 'isSickLeave', "Больничный");
              isSickLeave = info.feature;
              intervalNote = intervalNote === "" ? info.intervalNote : intervalNote;
              check = check && info.check;

              intervalNote = intervalNote === "" && !isAppointment ? "Врач не принимает" : intervalNote;

              let isBusy = false;
              this.records.forEach(function(record:PatientRecord) {
                if (day.fullDate.getDate() == new Date(record.interval.start).getDate() &&
                    day.fullDate.getMonth() == new Date(record.interval.start).getMonth() &&
                    day.fullDate.getFullYear() == new Date(record.interval.start).getFullYear() &&
                    moment(start.toDate()).diff(moment(new Date(0,0,0,new Date(record.interval.start).getHours(),new Date(record.interval.start).getMinutes(),0))) === 0 &&
                    record.spec.id == sch.spec.id) {
                      patients.push(record);
                  isBusy = true;
                }
              }, this);
              if (check) {
                if (isDontWork || isLearn || isDocWork || !isAppointment) {
                  // добавление второй строчки, если интервал не приемный
                  intervals.push({
                    interval: start.toString(),
                    intervalEnd: end.toString(),
                    date: new Date(day.fullDate.getFullYear(),day.fullDate.getMonth(),day.fullDate.getDate(),start.hour(),start.minute(),0),
                    spec: sch.spec,
                    name: intervalNote,
                    patients: patients,
                    isAppointment: isAppointment,
                    isDontWork: isDontWork,
                    isLearn: isLearn,
                    isDocWork: isDocWork,
                    isVacation: isVacation,
                    isHomeAppointment: isHomeAppointment,
                    isSickLeave: isSickLeave,
                    isBusy: isBusy
                  });               
                } else {
                  intervals.push({
                    interval: start.toString(),
                    intervalEnd: end.toString(),
                    date: new Date(day.fullDate.getFullYear(),day.fullDate.getMonth(),day.fullDate.getDate(),start.hour(),start.minute(),0),
                    spec: sch.spec,
                    name: "",
                    patients: patients,
                    isAppointment: isAppointment,
                    isDontWork: isDontWork,
                    isLearn: isLearn,
                    isDocWork: isDocWork,
                    isVacation: isVacation,
                    isHomeAppointment: false,
                    isSickLeave: false,
                    isBusy: isBusy
                  });
                }
              } else if (isBusy) {
                // если на время записан пациент, выводим в любом случае
                intervals.push({
                  interval: start.toString(),
                  intervalEnd: end.toString(),
                  date: new Date(day.fullDate.getFullYear(),day.fullDate.getMonth(),day.fullDate.getDate(),start.hour(),start.minute(),0),
                  spec: sch.spec,
                  name: "",
                  patients: patients,
                  isAppointment: false,
                  isDontWork: false,
                  isLearn: false,
                  isDocWork: false,
                  isVacation: false,
                  isHomeAppointment: false,
                  isSickLeave: false,
                  isBusy: isBusy
                });
              }  
            }

            this.columns.push({
              today: day.name,
              spec: sch.spec,
              position: sch.position,
              room: sch.room,
              startWorkTime: sch.startWorkTime,
              endWorkTime: sch.endWorkTime,
              days: sch.days,
              intervals: intervals
            });
          }
        }
      }, this)
    }, this)
  }
}
