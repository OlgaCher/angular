import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Timestamp } from 'rxjs';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    return {
      patients: [
        { id: 1, surname: 'Иванов', name: 'Иван', middlename: 'Иванович', date: '11.11.2011', policy: '1111111111111111' },
        { id: 2, surname: 'Алексеев', name: 'Алексей', middlename: 'Алексеевич', date: '22.12.1922', policy: '2222222222222222' },
        { id: 3, surname: 'Петров', name: 'Петр', middlename: 'Петрович', date: '01.01.1990', policy: '3333333333333333' },
        { id: 4, surname: 'Сергеев', name: 'Сергей', middlename: 'Сергеевич', date: '02.02.2002', policy: '4444444444444444' },
        { id: 5, surname: 'Васильев', name: 'Василий', middlename: 'Васильевич', date: '09.09.1949', policy: '5555555555555555' }
      ],

      positions: [
        {id: 1, name: 'Терапевт'},
        {id: 2, name: 'Офтальмолог'}
      ],

      specialists: [
        { id: 1, name: 'Григорьева Г.Г.', position: {id: 1, name: 'Терапевт'}},
        { id: 2, name: 'Сидорова С.С.', position: {id: 1, name: 'Терапевт'}},
        { id: 3, name: 'Елисеева Е.Е.', position: {id: 2, name: 'Офтальмолог'}},
        { id: 4, name: 'Константинова-Щедрина А.А.', position: {id: 2, name: 'Офтальмолог'}}
      ],

      schedule : [
        { 
          spec: {id: 1, name: 'Григорьева Г.Г.'}, 
          position: {id: 1, name: 'Терапевт'},
          room: 'ГП №128 к.110',
          startWorkTime: new Date(0,0,0,10,0,0),
          endWorkTime: new Date(0,0,0,20,0,0),
          days: [1,2,3,4,5],
          gridStep: 30,
          appointment: [{start: new Date(0,0,0,10,0,0), end: new Date(0,0,0,14,0,0), days: [1,2,3,4,5]},
                        {start: new Date(0,0,0,15,0,0), end: new Date(0,0,0,20,0,0), days: [1,2,3,4,5]}],
          dontWork: [{start: new Date(0,0,0,14,0,0), end: new Date(0,0,0,15,0,0), days: [1,2,3,4,5]}],
          learn: null,
          docWork: null,
          vacation: null,
          homeAppointment: null,
          sickLeave: null
        },
        { 
          spec: {id: 2, name: 'Сидорова С.С.'}, 
          position: {id: 1, name: 'Терапевт'},
          room: 'ГП №128 к.120',
          startWorkTime: new Date(0,0,0,8,0,0),
          endWorkTime: new Date(0,0,0,15,0,0),
          days: [1,2,3,4],
          gridStep: 30,
          appointment: [{start: new Date(0,0,0,10,0,0), end: new Date(0,0,0,15,0,0), days: [1,2,3,4]}],
          dontWork: null,
          learn:  [{start: new Date(0,0,0,10,0,0), end: new Date(0,0,0,15,0,0), days: [1]}],
          docWork: null,
          vacation: null,
          homeAppointment: null,
          sickLeave: null
        },
        { 
          spec: {id: 2, name: 'Сидорова С.С.'}, 
          position: {id: 1, name: 'Терапевт'},
          room: 'ГП №128 к.130',
          startWorkTime: new Date(0,0,0,14,0,0),
          endWorkTime: new Date(0,0,0,18,0,0),
          days: [5,6],
          gridStep: 10,
          appointment: [{start: new Date(0,0,0,10,0,0), end: new Date(0,0,0,14,0,0), days: [5,6]}],
          dontWork: null,
          learn: null,
          docWork: null,
          vacation: null,
          homeAppointment: null,
          sickLeave: null
        },
        { 
          spec: {id: 3, name: 'Елисеева Е.Е.'}, 
          position: {id: 2, name: 'Офтальмолог'},
          room: 'ГП №128 к.140',
          startWorkTime: new Date(0,0,0,8,0,0),
          endWorkTime: new Date(0,0,0,18,0,0),
          days: [1,2,3,4,5],
          gridStep: 30,
          appointment: [{start: new Date(0,0,0,10,0,0), end: new Date(0,0,0,17,45,0), days: [1,2,3,4,5]}],
          dontWork: null,
          learn: null,
          docWork: [{start: new Date(0,0,0,14,30,0), end: new Date(0,0,0,14,55,0), days: [1,2,3,4,5]},
                   {start: new Date(0,0,0,16,20,0), end: new Date(0,0,0,16,40,0), days: [1,2,3,4,5]}],
          vacation: null,
          homeAppointment: null,
          sickLeave: null
        },
        { 
          spec: {id: 4, name: 'Константинова-Щедрина А.А.'}, 
          position: {id: 2, name: 'Офтальмолог'},
          room: 'ГП №128 к.150',
          startWorkTime: new Date(0,0,0,9,0,0),
          endWorkTime: new Date(0,0,0,21,0,0),
          days: [2,3,4,5,6],
          gridStep: 30,
          appointment: [{start: new Date(0,0,0,9,0,0), end: new Date(0,0,0,21,0,0), days: [3,4,5,6]}],
          dontWork: null,
          learn: null,
          docWork: null,
          vacation: null,
          homeAppointment: null,
          sickLeave: null
        },
      ],

      records: [
        {
          id: 1,
          patient: {id: 1, surname: 'Иванов', name: 'Иван', middlename: 'Иванович', date: '11.11.2011', policy: '1111111111111111'},
          interval: {start: new Date(2020,1,24,10,0,0), end: null, days: []},
          spec: {id: 1, name: 'Григорьева Г.Г.'}
        },
        {
          id: 2,
          patient: {id: 2, surname: 'Алексеев', name: 'Алексей', middlename: 'Алексеевич', date: '22.12.1922', policy: '2222222222222222'},
          interval: {start: new Date(2020,1,24,10,0,0), end: null, days: []},
          spec: {id: 1, name: 'Григорьева Г.Г.'}
        },
        {
          id: 3,
          patient: {id: 3, surname: 'Петров', name: 'Петр', middlename: 'Петрович', date: '01.01.1990', policy: '3333333333333333'},
          interval: {start: new Date(2020,1,24,10,30,0), end: null, days: []},
          spec: {id: 1, name: 'Григорьева Г.Г.'}
        },
        {
          id: 4,
          patient: {id: 4, surname: 'Сергеев', name: 'Сергей', middlename: 'Сергеевич', date: '02.02.2002', policy: '4444444444444444'},
          interval: {start: new Date(2020,1,24,10,30,0), end: null, days: []},
          spec: {id: 2, name: 'Сидорова С.С.'}
        }        
      ]
    };
  }
}